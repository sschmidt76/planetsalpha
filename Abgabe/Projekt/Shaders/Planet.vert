//Author: Beatriz Bodas Roriguez
//Created: Jan. 2014



varying vec3 lightDir, normal;
attribute vec3 vertex;
attribute vec2 uv1;
uniform mat4 _mvProj;
varying vec2 uv;

void main()
{
	

	lightDir = normalize(vec3(gl_LightSource[0].position));
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
	normal = normalize(gl_NormalMatrix * gl_Normal);
}