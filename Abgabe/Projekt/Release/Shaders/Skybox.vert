//Author: Stefanie Schmidt
//Created: Jan. 2014

#version 120

void main() 
{
	gl_TexCoord[0].xyz = gl_Vertex.xyz;
	gl_Position = gl_ProjectionMatrix * vec4(mat3x3(gl_ModelViewMatrix) * vec3(gl_Vertex), 1.0);
}
