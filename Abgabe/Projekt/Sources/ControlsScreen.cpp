/*
 * ControlsScreen.cpp
 *
 *  Created on: 01.2014
 *      Author: Stefanie Schmidt
 */

#include "ControlsScreen.h"

/**
 * ControlsScreen --> Constructor inherits MenuScreen
 * adds the text that is shown on the screen
 * @param void
 */
ControlsScreen::ControlsScreen() : MenuScreen("Controls:")
{
	addItem("1. Back");
	addItem("w: move forward");
	addItem("a: move left");
	addItem("s: move backwards");
	addItem("d: move right");
	addItem("q: rotate left");
	addItem("e: rotate right");
	addItem("c: rotate up");
	addItem("v: rotate down");
}

ControlsScreen::~ControlsScreen(void)
{
}

/**
 * const Event Handler: go back to main menu

 * @param {MenuManager&} manager as reference
 * @param {int} index
 * @return {void}
 */
void ControlsScreen::onSelect(MenuManager& manager, int index) const
{
	switch (index)
	{
	case 1:
		manager.popScreen();
		break;
	}
}
