/*
 * GameObject.h
 * Authors: Beatriz Bodas Rodriguez, Stefanie Schmidt
 * Created: Nov. 2013
 */

#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include <GL/glew.h>
#include <GL/freeglut.h>
#include "tga.h"
#include "Vertex.h"
#include "shader.h"

class SceneGraph;

class GameObject
{
public:
	GameObject(void);
	virtual ~GameObject(void);
	virtual void update() = 0;
	void setPosition(float x, float y, float z);
	float x, y, z;
	void setSize(double radius);
	double radius;
	int slices, stack;
	void setSpeed(float x, float y, float z);
};

#endif