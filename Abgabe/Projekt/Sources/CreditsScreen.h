/*
 * CreditsScreen.h
 * Author: Stefanie Schmidt
 * Created: Jan.2014
 */

#ifndef __CREDITSSCREEN_H__
#define __CREDITSSCREEN_H__

#include "menuscreen.h"

class CreditsScreen : public MenuScreen
{
public:
	CreditsScreen(void);
	virtual ~CreditsScreen(void);
private:
	void onSelect(MenuManager& manager, int index) const;
};

#endif