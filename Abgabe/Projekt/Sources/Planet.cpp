/*
 * Planet.cpp
 *
 *  Created on: 11.2013
 *      Author: Beatriz B. Rodríguez
 */

#include "Planet.h"

GLuint planetShader;
GLuint planetTexture;
GLuint textureIdB = 0;

/**
 * Planet --> Constructor inherits Planet and it is added to the GameObject List
 * includes all information to build planets
 * @param void
 */
Planet::Planet(void):GameObject()
{
	
	this->x = 6.0f;
	this->y = 0.0f;
	this->z = 1.5f;
	this->rotationValue = 50;
	this->day = 5;
	this->planetTexture = 0;
	this->radius = 0.2;
	this->slices = 18;
	this->stack = 15;
	this->colorValueR = 1.0;
	this->colorValueG = 1.0;
	this->colorValueB = 1.0;
}

/**
 * Planet --> Constructor inherits Planet 
 * free access from other classes
 * @param float x, float y, float z, 
 * @param double radius, 
 * @param int slices int stac
 */
Planet::Planet(float x, float y, float z, double radius, int slices, int stack) //from GameObject basis class for the position and size & color manually information
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->radius = radius;
	this->slices = slices;
	this->stack = stack;
}

/**
 * Planet --> Destructor
 * @param void
 */
Planet::~Planet(void)
{
}

/**
 * initPlanet function loads vertex and fragment Shader
 * @return {void}
 */
void Planet::initPlanet()
{
	planetShader = Shader::loadShader("Sources/Planet.vert", "Sources/Planet.frag");

}

/**
 * GLuint function load textures for planets
 * @return textureIdB;
 */
GLuint Planet::loadPlanet()
{
	Image planetB;

	TGA::loadTGA(("Textures/Planets/TGA/WaterPlain0017_9_S.tga"), &planetB);
	
	glGenTextures(2, &textureIdB);
	glBindTexture(GL_TEXTURE_2D, textureIdB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, planetB.width, planetB.height, 0, GL_RGB, GL_UNSIGNED_BYTE, planetB.data ); 
	
	this->planetTexture = textureIdB;

	return textureIdB;
}

/**
 * update function with information about position, movement, rendering, connection to shader and texture binding
 * @param {void}
 * @return {void}
 */
void Planet::update()
{
	GLUquadric *quad;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	quad = gluNewQuadric();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, planetTexture);
	glUseProgram(planetShader);
	GLint loc = glGetUniformLocation(planetShader, "colorValue");
	glUniform3f(loc, colorValueR,colorValueG,colorValueB);

	day += (float)speed;
	GLfloat mat_shininess[] = {180.0};
	GLfloat light_position[] = {2.0, 0.0, 0.0};
	glShadeModel(GL_SMOOTH);
	GLfloat mat_specular[] = {0.0, 0.0, 1.0, 0.5};
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);

	glPushMatrix();
	glRotatef((GLfloat)day, 0.0f, 1.0f, 0.0f);
	glTranslatef(x, y, z);
	glRotatef(rotationValue, 0.0f, 1.0f, 0.0f);
	rotationValue+= 0.5;
	glColor3f(0.0f, 0.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, textureIdB);
	
	gluQuadricTexture(quad,1);
	gluSphere(quad, radius, slices, stack);
	glPopMatrix();
	glUseProgram(0);
	glDisable(GL_TEXTURE_2D);
}