/*
 * Skybox.cpp
 *
 * Modified: Stefanie Schmidt
 */

#include "Skybox.h"

#define LENGTH(x) sizeof(x)/sizeof(*x)

GLenum skyboxFaces[6];
GLuint skyboxShader;

//array for the 12 triangles of type Triangle
static Triangle skyboxTriangles[12] =
{
	Triangle(0, 3, 1),
	Triangle(0, 2, 3),
	Triangle(5, 6, 4),
	Triangle(5, 7, 6),
	Triangle(3, 6, 7),
	Triangle(3, 2, 6),
	Triangle(0, 5, 4),
	Triangle(0, 1, 5),
	Triangle(1, 7, 5),
	Triangle(1, 3, 7),
	Triangle(4, 2, 0),
	Triangle(4, 6, 2),
};

//vertex array for the vertex positions of the cubemap
static Vertex cubeVertices[8] =
{
	Vertex(-1.0f, -1.0f, -1.0f),
	Vertex(-1.0f, -1.0f,  1.0f),
	Vertex(-1.0f,  1.0f, -1.0f),
	Vertex(-1.0f,  1.0f,  1.0f),
	Vertex( 1.0f, -1.0f, -1.0f),
	Vertex( 1.0f, -1.0f,  1.0f),
	Vertex( 1.0f,  1.0f, -1.0f),
	Vertex( 1.0f,  1.0f,  1.0f),
};

/**
 * constructor --> initializes x, y, z and skyboxTexture to 0
 * @param {void}
 * @return {void}
 */
Skybox::Skybox(void): GameObject()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->skyboxTexture = 0;
}

/**
 * Skybox stand destructor
 * @param {void}
 * @return {void}
 */
Skybox::~Skybox(void)
{
}

/**
 * initSkybox initializes the skybox with its shaders
 * @param {void}
 * @return {void}
 */
void Skybox::initSkybox()
{
	skyboxShader = Shader::loadShader("Shaders/skybox.vert", "Shaders/skybox.frag");
}

/**
 * drawTriangles draws the cube's faces as triangles
 * @param {int} count
 * @param {Triangle*} triangles
 * @param {Vertex*} vertices
 * @return {void}
 */
void Skybox::drawTriangles(int count, Triangle* triangles, Vertex* vertices)
{
	glBegin(GL_TRIANGLES);
	{
		for (int i = 0; i < count; i++)
		{
			Triangle& triangle = triangles[i];
			Vertex& a = vertices[triangle.a];
			Vertex& b = vertices[triangle.b];
			Vertex& c = vertices[triangle.c];

			Vertex n = (b - a) % (c - a);

			glNormal3f(n.x, n.y, n.z);
			glVertex3f(a.x, a.y, a.z);
			glVertex3f(b.x, b.y, b.z);
			glVertex3f(c.x, c.y, c.z);
		}
	}
	glEnd();
}

/**
 * renderSkybox renders the cube map and loads the skybox shader
 * @param {void}
 * @return {void}
 */
void Skybox::renderSkybox()
{
	glEnable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);
	glUseProgram(skyboxShader);
	glColor3f(0.0f, 0.0f, 0.0f);
	drawTriangles(LENGTH(skyboxTriangles), skyboxTriangles, cubeVertices);

	glUseProgram(0);
	glDisable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
}

/** update must be implemented because it's virtual in base class
 * but the skybox is fix, so there's no update in the scene
 */
void Skybox::update()
{
}

/**
 * method that is called for each skybox face
 * @param {GLenum} target --> the actual face
 * @param {const Image&} image --> the actual image data as a const reference
 * @return {void}
 */
void Skybox::skyboxFace(GLenum target, const Image& image)
{
	glTexImage2D(target, 0, image.format, image.width, image.height, 0, image.format, GL_UNSIGNED_BYTE, image.data);
}

/**
 * loadSkybox loads the images before the skybox is initialized and rendered
 * @param {void}
 * @return {GLuint} textureID
 */
GLuint Skybox::loadSkybox()
{
	GLuint textureId;
	
	Image xPos;
	Image xNeg;
	Image yPos;
	Image yNeg;
	Image zPos;
	Image zNeg;


	//Loading different Textures by setting #if from 0 to 1.
	//Don't forget to set all other #if to 0 !!!
	
#if 0 //Nebula Purple
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_right1.tga"), &xNeg);
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_left2.tga"), &xPos);
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_top3.tga"), &yNeg);
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_bottom4.tga"), &yPos);
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_front5.tga"), &zNeg);
	TGA::loadTGA(("Textures/Skybox/Nebula/test_back6_back6.tga"), &zPos);
#endif

#if 1 //Green Nebula
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_right1.tga"), &xNeg);
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_left2.tga"), &xPos);
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_top3.tga"), &yNeg);
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_bottom4.tga"), &yPos);
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_front5.tga"), &zNeg);
	TGA::loadTGA(("Textures/Skybox/NebulaGreen/test_back6.tga"), &zPos);
#endif

#if 0 // Point Stars
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_right1.tga"), &xNeg);
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_left2.tga"), &xPos);
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_top3.tga"), &yNeg);
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_bottom4.tga"), &yPos);
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_front5.tga"), &zNeg);
	TGA::loadTGA(("Textures/Skybox/StarsPoint/Point Stars_back6.tga"), &zPos);
#endif

	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	skyboxFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X, xNeg);
	skyboxFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, xPos);
	skyboxFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, yNeg);
	skyboxFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, yPos);
	skyboxFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, zNeg);
	skyboxFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, zPos);

	this->skyboxTexture = textureId;

	return textureId;
}