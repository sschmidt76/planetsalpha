/*
 * SceneGraph.cpp
 *
 *  Created on: 11.2013
 *      Authors: Beatriz B. Rodríguez, Stefanie Schmidt
 */


#include "SceneGraph.h"

SceneGraph* sceneGraph = NULL;

/**
 * SceneGraph standard constructor with list of game objects
 * @param {void}
 * @return {void}
 */
SceneGraph::SceneGraph(void)
{
	gameObjects = std::vector<GameObject*>();
}

/**
 * SceneGraph standard destructor
 * @return {void}
 */
SceneGraph::~SceneGraph(void)
{
}

/**
 * updates the game objects and iterates through the list
 * @param {void}
 * @return {void}
 */
void SceneGraph::update()
{
	for(auto it = gameObjects.begin(); it!= gameObjects.end(); it++)
	{
		(*it)->update();
	}
}

/**
 * gets the instance of the singleton
 * @param {void}
 * @return {SceneGraph*}
 */
SceneGraph* SceneGraph::getInstance()
{
	if(sceneGraph)
	{
		return sceneGraph;
	}

	else
	{
		sceneGraph = new SceneGraph();
		return sceneGraph;
	}
}