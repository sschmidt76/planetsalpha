/*
 * ControlScreen.h
 * Author: Stefanie Schmidt
 * Created: Jan. 2014
 */

#ifndef __CONTROLSSCREEN_H__
#define __CONTROLSSCREEN_H__

#include "menuscreen.h"

class ControlsScreen : public MenuScreen
{
public:
	ControlsScreen(void);
	virtual ~ControlsScreen(void);
private:
	void onSelect(MenuManager& manager, int index) const;
};

#endif