/*
 * MenuScreen.cpp
 * Created on: 27.06.2013
 * Author: sreinck
 * Modified: 01.2014 by Stefanie Schmidt
 */

#include "MenuScreen.h"

/**
 * MenuScreen constructor
 * @param {const string} &name as reference
 * @return {void}
 */
MenuScreen::MenuScreen(const std::string& name)
{
}

/**
 * MenuScreen standard destructor
 */
MenuScreen::~MenuScreen()
{
}

/**
 * const update updates the screen
 * @param {MenuManager&} manager as reference
 * @param {const string&} input as const reference
 * @return {void}
 */
void MenuScreen::update(MenuManager& manager, const std::string& input) const
{
	std::stringstream stream(input);
	int index;
	stream >> index;
	onSelect(manager, index);
}

/**
 * textWidth defines the width shown text
 * @param {const string&} text as const reference
 * @return {int} width
 */
int textWidth(const std::string& text)
{
	int width = 0;
	for (const char* str = text.c_str(); *str; str++)
	{
		width += glutStrokeWidth(GLUT_STROKE_ROMAN, *str);
	}
	return width;
}

/**
 * drawText centers the text on the x axis
 * @param {const string&} text as const reference
 * @return {void}
 */
void drawText(const std::string& text)
{
	glPushMatrix();
	{
		glTranslatef(-700.0f, 0.0f, 0.0f);
		for (const char* str = text.c_str(); *str; str++)
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN, *str);
		}
	}
	glPopMatrix();
}

/**
 * render const renders the stored items
 * @param {void}
 * @return {void}
 */
void MenuScreen::render() const
{
	GLuint menuShader = Shader::loadShader("Shaders/Menu.vert", "Shaders/Menu.frag");
	glUseProgram(menuShader);

	float scale = 0.0011f;
	glLineWidth(3.0f);

	glPushMatrix();
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glScalef(scale,scale,scale);
		glTranslatef(0.0f, 600.0f, 0.0f);
		drawText(name);

		int i = 1;
		for (auto it = items.begin(); it != items.end(); it++)
		{
			glTranslatef(0.0f, -150.0f, 0.0f);

			std::stringstream stream;
			stream << *it;
			std::string text = stream.str();

			drawText(text);
			
			i++;
		}
	}
	glPopMatrix();

	glUseProgram(0);
}

/**
 * addItem adds the current item to the items list
 * @param {const string&} item
 * @return {void}
 */
void MenuScreen::addItem(const std::string& item)
{
	items.push_back(item);
}