/*
 * shader.h
 *
 *  Created on: 09.07.2013
 *      Author: sreinck
 */

#ifndef __SHADER_H__
#define __SHADER_H__

class Shader
{
public:
	static GLuint loadShader(const char* vertexShader, const char* fragmentShader);
};
#endif