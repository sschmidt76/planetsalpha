/*
 * Ship.cpp
 *
 *  Created on: 11/2013
 *      Author: Sebastian Pommerening
 */

#include "Ship.h"

GLuint shipShader;
GLuint shipTexture;
GLuint textureIdc = 2;

/**
 * Ship --> Constructor inherits Ship and it is added to the GameObject List
 * includes all information to build a Ship.
 * @param void
 */
Ship::Ship(void):GameObject()
{
	this->angle = 0.0f;
	this->shipTexture = 0;
}

/**
 * initShip function loads vertex and fragment Shader
 * @return {void}
 */
void Ship::initShip()
{
	shipShader = Shader::loadShader("Sources/Ship.vert", "Sources/Ship.frag");
	
}

/**
 * GLuint function load textures for the ship
 * @return GLuint textureIdc;
 */
GLuint Ship::loadShip()
{
	Image ship ;
	TGA::loadTGA(("Textures/Schiff/gebuerstetealuminiumtextur_512.tga"), &ship);

	glGenTextures(1, &textureIdc);
	glBindTexture(GL_TEXTURE_2D, textureIdc);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ship.width, ship.height, 0, GL_RGB, GL_UNSIGNED_BYTE, ship.data ); 
	
	this->shipTexture = textureIdc;

	return textureIdc;
}

/**
 * Ship --> Constructor inherits Ship 
 * free access from other classes
 * @param float x,
 * @param float y, 
 * @param float z, 
 */
Ship::Ship(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

/**
 * Ship --> Destructor
 * @param void
 */
Ship::~Ship(void)
{
}

/**
 * render function has not been used
 * @return {void}
 */
//void Ship::render()
//{
//	
//}

/**
 * rotateRight function to rotate the ship around its own y-axis
 * @param void
 */
void Ship::rotateRight()
{
	float dx=x*transformationsMatrix[0] + y*transformationsMatrix[1] + z*transformationsMatrix[2];
	float dy=x*transformationsMatrix[4] + y*transformationsMatrix[5] + z*transformationsMatrix[6];
	float dz=x*transformationsMatrix[8] + y*transformationsMatrix[9] + z*transformationsMatrix[10];
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadMatrixf(transformationsMatrix);
	this->angle-= 1.5f;
	//glRotatef(1.5f,0,1,0);
	glGetFloatv(GL_MODELVIEW_MATRIX, transformationsMatrix);
	glPopMatrix();
	//this->angle-= 1.5f;
}

/**
 * rotateLeft function to rotate the ship around its own y-axis
 * @param void
 */
void Ship::rotateLeft()
{
	glPushMatrix();
	//glLoadIdentity();

	this->angle+= 1.5f;
	//glLoadIdentity();
	glPopMatrix();
}

/**
 * update function with information about position, movement, rendering, connection to shader and texture binding
 * @param {void}
 * @return {void}
 */
void Ship::update()
{
	GLUquadric *quad;
 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	quad = gluNewQuadric();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, shipTexture);
	glUseProgram(shipShader);

	glColor3f(1,0,0);
	GLfloat vertices [] =	{-30, -5, 0,
							 -10, -5, 10,
							 +30, -5, 0,
							 -10, -5, -10,
							 -10, +5, 0};

	GLubyte indices [] = {0, 1, 4,
							1, 2, 4,
							2, 3, 4,
							3, 0, 4,
							0, 2, 1,
							0, 3, 2};

	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(angle,0,1,0);
	glScalef(0.05f,0.05f,0.05f);
	////
	glBindTexture(GL_TEXTURE_2D, textureIdc);
	gluQuadricTexture(quad,1);
	////
		glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer(3, GL_FLOAT, 0, vertices);
			glDrawElements(GL_TRIANGLES,18, GL_UNSIGNED_BYTE, indices);
		glDisableClientState(GL_VERTEX_ARRAY);
	glPopMatrix();

	////
	glUseProgram(0);
	glDisable(GL_TEXTURE_2D);
	////
}