/*
 * GameObject.cpp
 *
 *  Created on: 11.2013
 *      Author: Beatriz B. Rodr�guez, Stefanie Schmidt
 */


#include "GameObject.h"
#include "SceneGraph.h"

/**
 * GameObject --> Constructor inherits GameObject
 * pushing gameObjects into the SceneGraph::getInstance() List --> as Single Ton
 * @param void
 */
GameObject::GameObject(void)
{
	SceneGraph::getInstance()->gameObjects.push_back(this);
}

/**
 * GameObject --> Destructor
 * @return {void}
 */
GameObject::~GameObject(void)
{
}

/**
 * GameObject--> virtual update function is called each frame
 * must be implemented by all gameObjects
 * @param {void}
 * @return{void}
 */
void GameObject::update()
{
}

/**
 * setPosition positioning the objects in the scene
 * @param {float} x sets position on x axis
 * @param {float} y sets position on y axis
 * @param {float} z sets position on z axis
 * @return {void}
 */
 
void GameObject::setPosition(float x, float y, float z)//Seeting coordinates of game object�s position
{
	//public members for positioning
	this->x = x;
	this->y = y;
	this->z = z;
}

/**
 * setSize of the planets and sun
 * @param {double} radius the planets radius
 * @return {void}
 */
void GameObject::setSize(double radius)
{
	//public members radius, slices and stack
	this->radius = radius;

	//slices == the number of subdivisions around the Z axis (longitude)
	this->slices = 40;

	//stack == the number of subdivisions along the Z axis (latitude)
	this->stack = 20;
}