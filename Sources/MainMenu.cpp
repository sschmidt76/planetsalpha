/*
 * MainMenu.cpp
 *
 *  Created on: 27.06.2013
 *      Author: sreinck
		Modified: 01.2014 by Stefanie Schmidt
 */

#include "Callback.h"
#include "MainMenu.h"

/**
 * MainMenu --> Constructor inherits MenuScreen
 * adds the text that is shown on the screen
 * @param void
 */
MainMenu::MainMenu(): MenuScreen("Main Menu")
{
	addItem("1. Start");
	addItem("2. Credits");
	addItem("3. Controls");
	addItem("4. Quit");
}

MainMenu::~MainMenu()
{
}

/**
 * const Event Handler: handles user input and switches the scene

 * @param {MenuManager&} manager as reference
 * @param {int} index
 * @return {void}
 */
void MainMenu::onSelect(MenuManager& manager, int index) const
{
	switch (index)
	{
	case 1:
		{
			MenuManager::showSpace = true;
			reshape(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
		} break;
	case 2:
		{
			manager.pushScreen("Credits");
		} break;
	case 3:
		{
			manager.pushScreen("Controls");
		} break;
	case 4:
		{
			manager.popScreen();
		} break;
	default:
		break;
	}
}
