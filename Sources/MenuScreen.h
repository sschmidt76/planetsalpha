/*
 * MenuScreen.h
 *
 *  Created on: 27.06.2013
 *      Author: sreinck
 *  Modified: Stefanie Schmidt Dec. 2013
 */

#ifndef __MENUSCREEN_H__
#define __MENUSCREEN_H__

#include <GL\glew.h>
#include <GL\freeglut.h>

#include <iostream>
#include <sstream>
#include <list>
#include <string>

#include "MenuManager.h"
#include "shader.h"

class MenuScreen
{
public:
	MenuScreen(const std::string& name);
	virtual ~MenuScreen();
	void update(MenuManager& manager, const std::string& input) const;
	void render() const;
protected:
	void addItem(const std::string& item);
private:
	virtual void onSelect(MenuManager& manager, int index) const = 0;
	std::string name;
	std::list<std::string> items;
};

#endif