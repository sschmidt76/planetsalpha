/*
 * MenuManager.h
 *
 *  Created on: 27.06.2013
 *      Author: sreinck
 * Mofified: Stefanie Schmidt Dec. 2013
 */

#ifndef __MENUMANAGER_H__
#define __MENUMANAGER_H__

#include <GL\glew.h>
#include <GL\freeglut.h>

#include <map>
#include <stack>
#include <sstream>
#include <string>

class MenuScreen;

class MenuManager
{
public:
	static bool showSpace ;
	static MenuManager* getInstance();
	GLdouble currentMatrix[16];
	void registerScreen(const std::string& name, const MenuScreen* screen);
	void pushScreen(const std::string& name);
	void popScreen();
	static void keyPressed(unsigned char key, int x, int y);
	static void menuDisplay();
	static void menuReshape(int w, int h);
	bool update(const std::string& input);
	void render();
private:
	MenuManager();
	~MenuManager();
	typedef std::map<std::string, const MenuScreen*> ScreenMap;
	ScreenMap screens;
	std::stack<const MenuScreen*> screenStack;
	static MenuManager* instance;
	friend class Guard;
};

#endif