/*
 * Spawn.cpp
 *
 *  Created in 11.2013
 *      Author: Beatriz B. Rodríguez
 */

#include "Spawn.h"

/**
 * Spawn standard constructor
 */
Spawn::Spawn(void)
{
}

/**
 * Spawn standard destructor
 */
Spawn::~Spawn(void)
{
}

/**
 * Create spawns all objects except skybox
 * @param {void}
 * @return {void}
 */
void Spawn::create()
{
	Sun* sun = new Sun();
	sun->setPosition(2.0f, 0.0f, 0.0f);
	sun->initSun();
	sun->loadSun();

	Planet* planetA = new Planet();
	planetA->speed = 0.124f;
	planetA->setPosition(2.0f, 0.0f, 2.0f);
	planetA->setSize(0.2f);
	planetA->colorValueR = 1.0f;
	planetA->colorValueG = 0.5f;
	planetA->colorValueB = 0.9f;
	planetA->initPlanet();
	planetA->loadPlanet();
		
	Planet* planetB = new Planet();
	planetB->speed = 0.1f;
	planetB->setPosition(3.0f, 1.0f, 0.0f);
	planetB->setSize(0.4f);
	planetB->colorValueR = 1.0f;
	planetB->colorValueG = 0.0f;
	planetB->colorValueB = 0.0f;
	planetB->initPlanet();
	planetB->loadPlanet();

	Planet* planetC = new Planet();
	planetC->speed = 0.128f;
	planetC->setPosition(4.0f, 0.5f, 5.0f);
	planetC->setSize(0.3f);
	planetC->initPlanet();
	planetC->loadPlanet();

	Planet* planetD = new Planet();
	planetD->speed = 0.13f;
	planetD->setPosition(5.0f, 5.0f, 9.0f);
	planetD->setSize(0.2f);
	planetD->colorValueR = 0.5f;
	planetD->colorValueG = 0.5f;
	planetD->colorValueB = 0.0f;
	planetD->initPlanet();
	planetD->loadPlanet();
		
	Planet* planetE = new Planet();
	planetE->speed = 0.126;
	planetE->setPosition(9.0f, -1.0f, 12.0f);
	planetE->setSize(0.4f);
	planetE->colorValueR = 0.0f;
	planetE->colorValueG = 0.7f;
	planetE->colorValueB = 1.0f;
	planetE->initPlanet();
	planetE->loadPlanet();
	
	Planet* planetF = new Planet();
	planetF->speed = 0.132f;
	planetF->setPosition(17.0f, 3.5f, -5.0f);
	planetF->setSize(0.3f);
	planetF->colorValueR = 0.5f;
	planetF->colorValueG = 0.0f;
	planetF->colorValueB = 0.5f;
	planetF->initPlanet();
	planetF->loadPlanet();

	Planet* planetG = new Planet();
	planetG->speed = 0.136f;
	planetG->setPosition(8.0f, 6.0f, -9.0f);
	planetG->setSize(0.3f);
	planetG->colorValueR = 0.5f;
	planetG->colorValueG = 0.5f;
	planetG->colorValueB = 0.5f;
	planetG->initPlanet();
	planetG->loadPlanet();

	Planet* planetH = new Planet();
	planetH->speed = 0.119f;
	planetH->setPosition(9.5f, -0.5f, 12.0f);
	planetH->setSize(0.3f);
	planetH->initPlanet();
	planetH->loadPlanet();
}