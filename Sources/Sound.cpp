#include "Sound.h"


Sound::Sound(void)
{
	backgroundMusic = NULL;
}

Sound::~Sound(void)
{
}

bool Sound::initSound()
{
	if (SDL_Init(SDL_INIT_AUDIO) == -1)
	{
		return false;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
	{
		printf("Open audio failed");
		return false;
	}

	return true;
}

bool Sound::loadSounds()
{
	backgroundMusic = Mix_LoadMUS("203674__rjonesxlr8__mystic-crystals.wav");
	if (backgroundMusic == NULL)
	{
		printf("Memory allocation failure");
		return false;
	}

	return true;
}

void Sound::playMusic()
{
	Mix_PlayMusic(backgroundMusic, -1);
}

//void Sound::playChunk(char filePath)
//{
//	
//}