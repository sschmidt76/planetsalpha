/*
 * Vertex.c
 *
 *  Created on: 09.07.2013
 *      Author: sreinck
 */

#include "Vertex.h"


Vertex::Vertex(void)
{
	this->x = 0;
	this->y = 0;
	this->z = 0;

}

Vertex::Vertex(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;

}
Vertex::~Vertex(void)
{
}


Vertex Vertex::operator/(float d) const
{
	return Vertex(x / d, y / d, z / d);
}

float Vertex::length() const
{
	return sqrt(x * x + y * y + z * z);
}

Vertex Vertex::norm() const
{
	return (*this) / length();
}

Vertex operator+(const Vertex& a, const Vertex& b)
{
	return Vertex(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vertex operator-(const Vertex& a, const Vertex& b)
{
	return Vertex(a.x - b.x, a.y - b.y, a.z - b.z);
}

Vertex operator*(float s, const Vertex& a)
{
	return Vertex(s * a.x, s * a.y, s * a.z);
}

Vertex operator%(const Vertex& a, const Vertex& b)
{
	Vertex c;

	c.x = a.y * b.z - a.z * b.y;
	c.y = a.z * b.x - a.x * b.z;
	c.z = a.x * b.y - a.y * b.x;

	return c;
}