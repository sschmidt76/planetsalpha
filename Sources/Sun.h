/*
 * Sun.h
 * Authors: Beatriz Bodas Rodriguez
 * Created: Nov. 2013
 */

#ifndef __SUN_H__
#define __SUN_H__

#include "GameObject.h"

class Sun : public GameObject
{
public:
	Sun(void);
	virtual ~Sun(void);
	void update();
	void initSun();
	GLuint sunTexture;
	GLuint loadSun();
private:
	float year;
};

#endif
