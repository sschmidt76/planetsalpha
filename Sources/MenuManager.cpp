/*
 * MenuManager.cpp
 *
 *  Created on: 27.06.2013
 *      Author: sreinck
		Modified: 01.2014 by Stefanie Schmidt
 */

#include "MenuManager.h"
#include "MenuScreen.h"

/**
 * Guard deletes the MenuManager instance safely
 */
class Guard
{
public:
	~Guard()
	{
		if (MenuManager::instance != 0)
		{
			delete MenuManager::instance;
		}
	}
};

bool MenuManager::showSpace = false;
MenuManager* MenuManager::instance = 0;

/**
 * getInstance creates a new MenuManager instance
 * @param {void}
 *return {MenuManager*}
 */
MenuManager* MenuManager::getInstance()
{
	static Guard guard;
	if (instance == 0)
	{
		instance = new MenuManager();
	}
	return instance;
}

MenuManager::MenuManager()
{
}

/**
 * ~MenuManager destructor deletes the second stored value in the screenMap
 */
MenuManager::~MenuManager()
{
	for (auto it = screens.begin(); it != screens.end(); it++)
	{
		delete it->second;
	}
}

/**
 * registerScreen registers the current screen to the map
 * @param {const string&} name as reference
 * @param {const MenuScreen*} screen as pointer
 * @return {void}
 */
void MenuManager::registerScreen(const std::string& name, const MenuScreen* screen)
{
	screens[name] = screen;
}

/**
 * pushScreen pushes the second stored value on the screenStack
 * @param {const string&} name as reference
 * @return {void}
 */
void MenuManager::pushScreen(const std::string& name)
{
	ScreenMap::iterator it = screens.find(name);

	if (it != screens.end())
	{
		screenStack.push(it->second);
	}
}

/**
 * popScreen pops the last screen off the stack
 * @param {void}
 * @return {void}
 */
void MenuManager::popScreen()
{
	screenStack.pop();

}

/**
 * update updates the stack when a key is pressed
 * @param {const string&} input as a const reference
 * @return {bool} screenStack.size if an update was made, otherwise false --> VERIFY!!
 */
bool MenuManager::update(const std::string& input)
{
	if (screenStack.size() > 0)
	{
		screenStack.top()->update(*this, input);
		return screenStack.size() > 0;
	}

	return false;
}

/**
 * render renders the fist screen on the stack
 * @param {void}
 * @return {void}
 */
void MenuManager::render()
{
	if (screenStack.size() > 0)
	{
		screenStack.top()->render();
	}
}

/**
 * keyPressed callback function for keyboard input
 * @param {unsigned char} key the pressed key, must be of type unsigned char for glutKeyboardFunc
 * @param {int} x parameter is needed for glutKeyboardFunc
 * @param {int} y parameter is needed for glutKeyboardFunc
 * @return {void}
 */
void  MenuManager::keyPressed(unsigned char key, int x, int y)
{
	MenuManager* manager = MenuManager::getInstance();

	std::stringstream stream;

	stream << key;

	if (!manager->update(stream.str()))
	{
		exit(0);
	}
}

/**
 * menuDisplay callback function to display the menu with OpenGL
 * @param {void}
 * @return {void}
 */
void MenuManager::menuDisplay()
{
	MenuManager* manager = MenuManager::getInstance();

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	manager->render();

	glutSwapBuffers();
}

/**
 * menuReshape callback function to reshape the window
 * @param {int} w window width
 * @param {int} h window height
 * @return {void}
 */
void  MenuManager::menuReshape(int w, int h)
{
	for(int i= 0; i < 16; i++) 
	{
		MenuManager::getInstance()->currentMatrix[i] = 0;
	}

	GLdouble aspect =(GLdouble)w/(GLdouble)h;
	glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();

	glOrtho(-aspect,aspect,-1.0,1.0,0.0,1.0);
}