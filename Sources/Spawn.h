/*
 * Spawn.h
 * Authors: Beatriz Bodas Rodriguez
 * Created: Feb. 2013
 */

#ifndef __SPAWN_H__
#define __SPAWN_H__

#include <vector>
#include <fstream>

#include "Planet.h"
#include "GameObject.h"
#include "SceneGraph.h"
#include "Sun.h"

class Spawn
{
public:
	Spawn(void);
	virtual ~Spawn(void);
	void create();
};

#endif