/*
 * Author: Beatriz Bodas Rodriguez
 * Created: Jan. 2014
 */

varying vec3 lightDir, normal;

void main()
{
	normal = normalize(gl_NormalMatrix * gl_Normal);

	//choose the typ of the light
	for(int i = 1; i < 2; i++) 
	{
		lightDir = normalize(vec3(gl_LightSource[i].position.xyz));
	}
	gl_TexCoord[0] = gl_MultiTexCoord0;

	gl_Position = ftransform();
}