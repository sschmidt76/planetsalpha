/*
 * Ship.h
 * Authors: Sebastian Pommerening
 * Created: Dec. 2013
 */

#ifndef __SHIP_H__
#define __SHIP_H__

#include "GameObject.h"

class Ship : public GameObject
{
public:
	Ship(void);
	Ship(float x, float y, float z);
	virtual ~Ship(void);

	void initShip();
	void update();
	void rotateRight();
	void rotateLeft();

	GLuint shipTexture;
	GLuint loadShip();

private:
	float angle;
	float transformationsMatrix[16];
};

#endif
