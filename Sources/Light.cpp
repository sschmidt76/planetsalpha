/*
 * Light.cpp
 *
 *  Created on: 01.2014
 *      Author: Beatriz B. Rodríguez
 */


#include "Light.h"

/**
 * Light --> Constructor inherits Light
 * adds the light information for ambient and directional
 * @param void
 */
Light::Light(void)
{
}

/**
 * Light --> Destructor
 * @param void
 */
Light::~Light(void)
{
}

/**
 * Light information: position, specular and ambient
 * @param {void}
 * @return {void}
 */
void Light::sunLight()
{
	const GLfloat pos[] = { 2.0, 0.0f, 0.5f, 1.0f };
	const GLfloat pos2[] = { 0.0, 0.0f, 190.5f, 1.0f };
	const GLfloat pos3[] = { 0.0, 0.0f, -190.5f, 1.0f };
	GLfloat specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat ambient = 1.0;

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightf(GL_LIGHT0, GL_INTENSITY, 10);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT1);
	glLightf(GL_LIGHT1, GL_INTENSITY, 10);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT2);
	glLightf(GL_LIGHT2, GL_INTENSITY, 10);

	glLightf(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

	glLightf(GL_LIGHT1, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_POSITION, pos2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular);

	glLightf(GL_LIGHT2, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT2, GL_POSITION, pos3);
	glLightfv(GL_LIGHT2, GL_SPECULAR, specular);
}