/*
 * tga.c
 *
 *  Created on: 09.07.2013
 *      Author: sreinck
 */


#include "tga.h"

#include <GL/glu.h>

#include <stdio.h>
#include <stdlib.h>

struct TGAHeader
{
	GLubyte lenID;
	GLubyte typePalette;
	GLubyte typeImage;
	GLushort startPalette;
	GLushort lenPalette;
	GLubyte sizePaletteEntry;
	GLushort startX;
	GLushort startY;
	GLushort width;
	GLushort height;
	GLubyte bitsPerPixel;
	GLubyte attrImage;
};

static void copyPixel(GLubyte * data, int pos, GLubyte * pixel, int components)
{
	if (components < 3)
	{
		data[pos++] = pixel[0];
	}
	else
	{
		data[pos++] = pixel[2];
		data[pos++] = pixel[1];
		data[pos++] = pixel[0];
		if (components > 3)
		{
			data[pos++] = pixel[3];
		}
	}
}

#define BOTTOMUP(header) (!((header).attrImage & 32))

void nextPixel(struct TGAHeader* header, int *pos)
{
	int components = header->bitsPerPixel / 8;
	*pos += components;
	if (BOTTOMUP(*header) && *pos % (header->width * components) == 0)
	{
		*pos -= 2 * header->width * components;
	}
}

static GLubyte readByte(FILE* file)
{
	return fgetc(file);
}

static void readBytes(FILE* file, int count, GLubyte* value)
{
	fread(value, 1, count, file);
}

static GLushort readShort(FILE* file)
{
  int lower = readByte(file);
  int upper = readByte(file);
  return (upper << 8) | lower;
}

static GLboolean readHeader(FILE* file, struct TGAHeader* header)
{
  header->lenID            = readByte(file);
  header->typePalette      = readByte(file);
  header->typeImage        = readByte(file);
  header->startPalette     = readShort(file);
  header->lenPalette       = readShort(file);
  header->sizePaletteEntry = readByte(file);
  header->startX           = readShort(file);
  header->startY           = readShort(file);
  header->width            = readShort(file);
  header->height           = readShort(file);
  header->bitsPerPixel     = readByte(file);
  header->attrImage        = readByte(file);

  return GL_TRUE;
}

static const char* internLoadTGA(FILE* file, struct Image* image)
{
	struct TGAHeader header;
	int compressed;
	int size;
	int pixels;

	if (!readHeader(file, &header))
	{
		return "header";
	}

	if (header.lenID != 0)
	{
		return "ID";
	}

	if (header.typePalette != 0)
	{
		return "Palette";
	}

	switch (header.typeImage)
	{
	case 10:
	case 11:
		compressed = 1;
		break;
	case 2:
		compressed = 0;
		return "uncompressed";
	default:
		return "Imagetype";
	}

	if (header.startX != 0 || header.startY != 0)
	{
		return "Offset";
	}

	image->width = header.width;
	image->height = header.height;

	switch (header.bitsPerPixel)
	{
	case 8:
		image->components = 1;
		image->format = GL_ALPHA;
		break;
	case 24:
		image->components = 3;
		image->format = GL_RGB;
		break;
	case 32:
		image->components = 4;
		image->format = GL_RGBA;
		break;
	default:
		return "Components";
	}

	pixels = image->width * image->height;
	size = pixels * image->components;

	image->data = (GLubyte*) malloc(size);

	if (!image->data)
	{
		return "malloc";
	}

	if (compressed)
	{
		int pos = 0;
		int pixel = 0;

		if (BOTTOMUP(header))
		{
			pos = (image->height - 1) * image->width * image->components;
		}

		while (pixel < pixels)
		{
			int control = readByte(file);
			if (control >> 7)
			{
				int repeat = (control & 0x7f) + 1;
				GLubyte value[4];
				int i;

				readBytes(file, image->components, value);

				for (i = 0; i < repeat; i++)
				{
					copyPixel(image->data, pos, value, image->components);
					nextPixel(&header, &pos);
					pixel++;
				}
			}
			else
			{
				int plainbytes = ((control & 0x7f) + 1);
				GLubyte value[4];
				int i;

				for (i = 0; i < plainbytes; i++)
				{
					readBytes(file, image->components, value);
					copyPixel(image->data, pos, value, image->components);
					nextPixel(&header, &pos);
					pixel++;
				}
			}
		}
	}
	else
	{
		return "data";
	}

	return NULL;
}

GLuint TGA::toTexture(struct Image* image, GLboolean mipmapping)
{
	GLuint id;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	if (mipmapping)
	{
		gluBuild2DMipmaps(GL_TEXTURE_2D, image->format, image->width, image->height, image->format, GL_UNSIGNED_BYTE, image->data);
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, image->format, image->width, image->height, 0, image->format, GL_UNSIGNED_BYTE, image->data);
	}

	free(image->data);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	if (mipmapping)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	return id;
}

const char* TGA::loadTGA(const char* filename, struct Image* image)
{
	FILE* file = fopen(filename, "rb");
	const char *error;

	if (file)
	{
		error = internLoadTGA(file, image);
		fclose(file);
	}
	else
	{
		error = "open";
	}

	return error;
}
