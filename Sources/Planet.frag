/*
 * Author: Beatriz Bodas Rodriguez
 * Created: Jan. 2014
 */

varying vec3 lightDir, normal;
uniform sampler2D tex;
uniform vec3 colorValue;

void main()
 {
	vec3 ct, cf;
	vec4 texel;
	float intensity, at, af;
	//normalizing light direction for the intensity
	intensity = max(dot(lightDir, normalize(normal)),0.0);

	//intensity regarding diffuse and ambient light
	cf = intensity * (gl_FrontMaterial.diffuse).rgb + gl_FrontMaterial.ambient.rgb;

	//material of the texture diffuse
	af = gl_FrontMaterial.diffuse.a;
	texel = texture2D(tex, gl_TexCoord[0].st);

	ct = texel.rgb;
	at = texel.a;
	//light information for all pixels
	gl_FragColor = vec4 (ct*cf * colorValue, at*af) ;
	
}
          
