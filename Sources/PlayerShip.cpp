/*
 * PlayerShip.cpp
 *
 *  Created on: 01.12.2013
 *      Author: Stefanie Schmidt
 */

#include "PlayerShip.h"


PlayerShip::PlayerShip(void) : GameObject()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}


PlayerShip::~PlayerShip(void)
{
}

void PlayerShip::render()
{
	/*glPushMatrix();
	glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
	//glTranslatef(x, y, z);
	glBegin(GL_TRIANGLES);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.7f, 0.0f, 0.0f);
		glVertex3f(0.6f, 0.2f, 0.0f);
		/*glVertex2f(0.0f, 0.0f);
		glVertex2f(0.8f, 0.0f);
		glVertex2f(0.4f, 0.8f);*/
	/*glEnd();
	glPopMatrix();*/
}

void PlayerShip::update()
{
	//implemented a solid sphere, because a triangle doesn't work in 3D
	glPushMatrix();
	glTranslatef(x, y, z);
	glColor3f(0.0f, 0.0f, 0.0f);
	glutSolidSphere(0.2, 10, 8);

	glPopMatrix();
}