/*
 * main.cpp
 *
 *  Created on: 11.2013
 *      Authors: Beatriz B. Rodríguez, Stefanie Schmidt,
 */

#include "Callback.h"
#include "Light.h"

Light* light;

/**
 * mainLoop runs until exit
 * @param {int} val is needed glutTimerFunc
 * @return {void}
 */
void mainLoop (int val)
{
	if(MenuManager::showSpace) 
	{
		glClearColor(0.0,0,0,0);
		glutReshapeFunc(reshape);
		glutKeyboardFunc(handleInput);
		glutKeyboardUpFunc(handleInputUp);
		display();
	}
	else if(!MenuManager::showSpace) 
	{
		MenuManager::menuDisplay();
	}

	glutTimerFunc (1000/60, mainLoop, 0);
}

/**
 * main entry point
 * @param {int} argc for console access
 * @param {char**} argv for console access as double pointer
 * @return {int} 0 if no error occured
 */
int main (int argc, char** argv)
{	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(1024, 1024);
	glutCreateWindow ("Planets Alpha");

	glewInit();

	init();
	light->sunLight();
	
	if (!MenuManager::showSpace)
	{
		showMenu();
	}
	
	glutDisplayFunc(display);
	glutTimerFunc (1000/60, mainLoop, 0);
	
	glutMainLoop();
	return 0;
}