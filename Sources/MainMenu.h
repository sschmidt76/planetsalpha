/*
 * MainMenu.h
 *
 *  Created on: 27.06.2013
 *      Author: sreinck
	Modified: Stefanie Schmidt Dec. 2013
 */

#ifndef __MAINMENU_H__
#define __MAINMENU_H__

#include "Callback.h"
#include "MenuScreen.h"
#include "SceneGraph.h"

class MainMenu: public MenuScreen
{
public:
	MainMenu();
	virtual ~MainMenu();
private:
	void onSelect(MenuManager& manager, int index) const;
};

#endif