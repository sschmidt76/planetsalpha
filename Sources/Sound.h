#pragma once
#include <SDL.h>
#include <SDL_mixer.h>

class Sound
{
private:
	Mix_Music* backgroundMusic;

public:
	Sound(void);
	virtual ~Sound(void);

	bool initSound();
	bool loadSounds();

	void playMusic();
	//void playChunk();
};

