/*
 * Callback.cpp
 *
 *  Created on: 11.2013
 *      Authors: Beatriz B. Rodríguez, Stefanie Schmidt,
				 Sebastian Pommerening
 */

#include "Callback.h"

#pragma comment(lib, "irrKlang.lib")

using namespace irrklang;

Skybox* skybox = new Skybox();

Camera cam;
float distance;
float xSpeed;
float ySpeed;
float zSpeed;
float stepVal;
float angleVal;

/**
 * init for initialization the scene
 * @param {void}
 * @return {void}
 */
void init ()
{	
	Spawn spawn;
	spawn.create();

	ISoundEngine* engine = createIrrKlangDevice();
	engine->play2D("Sounds/Stellardrone_AMomentOfStillness.mp3", true);

	skybox->loadSkybox();
	skybox->initSkybox();
	
	cam.position[0] = 5.0f;
	cam.position[1] = 0.0f;
	cam.position[2] = 2.5f;

	distance = 0.00f;
	xSpeed = 0.0f;
	ySpeed = 0.0f;
	zSpeed = 0.0f;
	stepVal = 0.003f;
	angleVal = 1.0f;

	skybox->renderSkybox();
	glEnable(GL_CULL_FACE);
}

/**
 * reshape is called when the window is reshaped
 * @param {int} w --> window width
 * @param {int} h --> window height
 * @return {void}
 */
void reshape (int w, int h)
{
   glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity ();
   gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 0.01, 100.0);
 }

/**
 * display for displaying the scene on the screen
 * @param {void}
 * @return {void}
 */
void display()
{
	if (MenuManager::showSpace)
	{
		glClearColor(0.0, 0.0, 0.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		cam.setView();
	
		skybox->renderSkybox();
			
		SceneGraph::getInstance()->update();
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_DEPTH_TEST);

		glutSwapBuffers();
	}
}

/**
 *handleInput handles keyboard input
 * @param {unsigned char} key
 * @param {int} x mouse position
 * @param {int} y mouse position
 * @return {void}
 */
void handleInput(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'w':
		{
			if (distance<1.0f)
			{
				zSpeed+=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}

			cam.moveLoc(0,0,1,distance);
		} break;
	case 's':
		{
			if (distance<1.0f)
			{
				zSpeed-=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}
			cam.moveLoc(0,0,1,-distance);
		} break;
	case 'a':
		{
			if (distance<1.0f)
			{
				xSpeed-=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}
			cam.moveLoc(1,0,0,-distance);
		} break;
	case 'd':
		{
			if (distance<1.0f)
			{
				xSpeed+=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}
			cam.moveLoc(1,0,0,distance);
		} break;
	case 'x':
		{
			if (distance<1.0f)
			{
				ySpeed+=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}
			cam.moveLoc(0,1,0,distance);
		} break;
	case 'y':
		{
			if (distance<1.0f)
			{
				ySpeed-=stepVal;
				distance+=stepVal; 
			}
			else
			{
				distance=1.0f;
			}
			cam.moveLoc(0,1,0,-distance);
		} break;
	case 'q':
		{
			cam.rotateLoc(-angleVal,0,1,0);
		} break;
	case 'e':
		{
			cam.rotateLoc(angleVal,0,1,0);
		} break;
	case 'c':
		{
			cam.rotateLoc(angleVal,1,0,0);
		} break;
	case 'v':
		{
			cam.rotateLoc(-angleVal,1,0,0);
		} break;
	case 27:
		{
			glMatrixMode (GL_PROJECTION);
			glLoadMatrixd(MenuManager::getInstance()->currentMatrix);
			MenuManager::getInstance()->menuReshape(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
			MenuManager::getInstance()->popScreen();
			MenuManager::getInstance()->render();
			MenuManager::showSpace = false;
			display();
			showMenu();

			MenuManager::menuDisplay();
		}
	}
}

/**
 *handleInputUp handles key releases
 * @param {unsigned char} key
 * @param {int} x mouse position
 * @param {int} y mouse position
 * @return {void}
 */
void handleInputUp(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'w':
		{
			if (zSpeed>0.0f)
			{
				for(zSpeed; zSpeed>0.0f; zSpeed-=stepVal)
				{
					distance-=stepVal;
				}
			}
			else
			{
				zSpeed = 0.0f;
			}
		} break;
	case 's':
		{
			if (zSpeed<0.0f)
			{
				for(zSpeed; zSpeed<0.0f; zSpeed+=stepVal)
				{
					distance-=stepVal;
				}
			}
			else
			{
				zSpeed = 0.0f;
			}
		} break;
	case 'a':
		{
			if (xSpeed<0.0f)
			{
				for(xSpeed; xSpeed<0; xSpeed+=stepVal)
				{
					distance-=stepVal;
				}
			}
		} break;
	case 'd':
		{
			if (xSpeed>0.0f)
			{
				for(xSpeed; xSpeed>0.0f; xSpeed-=stepVal)
				{
					distance-=stepVal;
				}
			}
		} break;
	case 'x':
		{
			if (ySpeed>0.0f)
			{
				for(ySpeed; ySpeed>0.0f; ySpeed-=stepVal)
				{
					distance-=stepVal;
				}
			}
		} break;
	case 'y':
		{
			if (ySpeed<0.0f)
			{
				for(ySpeed; ySpeed<0.0f; ySpeed+=stepVal)
				{
					distance-=stepVal;
				}
			}			
		} break;
	case 'q':
		{
			//empty
		} break;
	case 'e':
		{
			//empty
		} break;
	case 'c':
		{
			//empty
		} break;
	case 'v':
		{
			//empty
		} break;
	default:
		break;
	}
}

/**
 *showMenu
 * @param {void}
 * @return {void}
 */
void showMenu()
{
	MenuManager* manager = MenuManager::getInstance();

		manager->registerScreen("MAIN", new MainMenu());
		manager->registerScreen("Credits", new CreditsScreen());
		manager->registerScreen("Controls", new ControlsScreen());

		manager->pushScreen("MAIN");
		glutKeyboardFunc(MenuManager::keyPressed);
		glutReshapeFunc(MenuManager::menuReshape);
}