/*
 * shader.c
 *
 *  Created on: 09.07.2013
 *      Author: sreinck
 */

#include <GL/glew.h>

#include <stdio.h>
#include <stdlib.h>

#include "shader.h"

void printInfoLog(GLhandleARB obj)
{
	int infologLength = 0;
	int charsWritten = 0;
	char* infoLog;

	glGetObjectParameterivARB(obj, GL_OBJECT_INFO_LOG_LENGTH_ARB, &infologLength);

	if (infologLength > 0)
	{
		infoLog = (char*) malloc(infologLength);
		glGetInfoLogARB(obj, infologLength, &charsWritten, infoLog);
		printf("%s\n", infoLog);
		fflush(stdout);
		free(infoLog);
	}
}

char *textFileRead(const char *fn)
{
	FILE *fp;
	char *content = NULL;//(char*)malloc(sizeof(char));

	int count = 0;

	if (fn != NULL )
	{
		fp = fopen(fn, "rt");

		if (fp != NULL )
		{

			fseek(fp, 0, SEEK_END);
			count = ftell(fp);
			rewind(fp);

			if (count > 0)
			{
				content = (char *) malloc(sizeof(char) * (count + 1));
				count = fread(content, sizeof(char), count, fp);
				content[count] = '\0';
			}
			fclose(fp);
		}
	}
	return content;
}

GLuint Shader::loadShader(const char* vertexShader, const char* fragmentShader)
{
	char* vs = (char*)malloc(sizeof(char));
	char* fs = (char*)malloc(sizeof(char));

	GLuint v = 0, f = 0, p = 0;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	vs = textFileRead(vertexShader);
	fs = textFileRead(fragmentShader);

	{
		const char* vv = (char*)malloc(sizeof(char));
		vv = vs;
		const char* ff = (char*)malloc(sizeof(char));
		ff = fs;

		glShaderSource(v, 1, &vv, NULL);
		glShaderSource(f, 1, &ff, NULL );
	}

	free(vs);
	free(fs);

	glCompileShader(v);
	printInfoLog(v);

	glCompileShader(f);
	printInfoLog(f);

	p = glCreateProgram();

	glAttachShader(p, v);
	glAttachShader(p, f);

	glLinkProgram(p);

	return p;
}