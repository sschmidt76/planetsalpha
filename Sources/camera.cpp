/*
 * Author: Alexander Festini
 * Modified: Sebastian Pommerening
 */

#include "Camera.h"

/*
The constructor will set the matrix to identity, except the inverted z-axis.
I cant get used to "forward" being negative, but if this bothers you feel free
to change the marked parts.

Also, if you dont like to access the vectors like float-arrays you can always
use vector-pointers, as long as they only contain three floats like this:
struct vector{float x,y,z;};
and replace the float* with vector* in the header file.
*/
Camera::Camera(float x, float y, float z) {
	memset(transform, 0, 16*sizeof(float));
	transform[0] = 1.0f;
	transform[5] = 1.0f;
	transform[10] = -1.0f;
	transform[15] = 1.0f;
	transform[12] = x; transform[13] = y; transform[14] = z;

	right=&transform[0];
	up=&transform[4];
	forward=&transform[8];
	position=&transform[12];
}

Camera::~Camera() {}

/*
This one does pretty much the same as gluLookAt, just that it doesnt require
to extract the vectors for gluLookAt and have it rebuild the matrix we already
got.
*/
void Camera::setView() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	float viewmatrix[16]={//Remove the three - for non-inverted z-axis
						  transform[0], transform[4], -transform[8], 0,
						  transform[1], transform[5], -transform[9], 0,
						  transform[2], transform[6], -transform[10], 0,

						  -(transform[0]*transform[12] +
						  transform[1]*transform[13] +
						  transform[2]*transform[14]),

						  -(transform[4]*transform[12] +
						  transform[5]*transform[13] +
						  transform[6]*transform[14]),

						  //add a - like above for non-inverted z-axis
						  (transform[8]*transform[12] +
						  transform[9]*transform[13] +
						  transform[10]*transform[14]), 1};
	glLoadMatrixf(viewmatrix);
}

void Camera::moveLoc(float x, float y, float z, float distance) {
	float dx=x*transform[0] + y*transform[4] + z*transform[8];
	float dy=x*transform[1] + y*transform[5] + z*transform[9];
	float dz=x*transform[2] + y*transform[6] + z*transform[10];
	transform[12] += dx * distance;
	transform[13] += dy * distance;
	transform[14] += dz * distance;
}

void Camera::moveGlob(float x, float y, float z, float distance) {
	transform[12] += x * distance;
	transform[13] += y * distance;
	transform[14] += z * distance;
}

/*
Here we let OpenGls (most likely quite optimized) functions do the work.
Note that its transformations already are in local coords.
*/
void Camera::rotateLoc(float deg, float x, float y, float z) {
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadMatrixf(transform);
	glRotatef(deg, x,y,z);
	glGetFloatv(GL_MODELVIEW_MATRIX, transform);
	glPopMatrix();
}

/*
We have to invert the rotations to get the global axes in local coords.
Luckily thats just the transposed in this case.
*/
void Camera::rotateGlob(float deg, float x, float y, float z) {
	float dx=x*transform[0] + y*transform[1] + z*transform[2];
	float dy=x*transform[4] + y*transform[5] + z*transform[6];
	float dz=x*transform[8] + y*transform[9] + z*transform[10];
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadMatrixf(transform);
	glRotatef(deg, dx,dy,dz);
	glGetFloatv(GL_MODELVIEW_MATRIX, transform);
	glPopMatrix();
}