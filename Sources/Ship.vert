/*
 * Author: Sebastian Pommerening
 * Created: Jan. 2014
 */

varying vec3 lightDir, normal;

void main()
{
	normal = normalize(gl_NormalMatrix * gl_Normal);

	for(int i = 1; i < 5; i++) 
	{
		lightDir = (vec3(gl_LightSource[1].position.xyz));
	}
	gl_TexCoord[0] = gl_MultiTexCoord0;

	gl_Position = ftransform();
}
