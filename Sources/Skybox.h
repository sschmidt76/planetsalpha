/*
 * Skybox.h
 * Author: Sven Reinck
 * Modified: Stefanie Schmidt
 */

#ifndef __SKYBOX_H__
#define __SKYBOX_H__

#include <cstdio>

#include "gameobject.h"

struct Triangle
{
	Triangle(GLubyte _a, GLubyte _b, GLubyte _c): a(_a), b(_b), c(_c) {}
	GLubyte a;
	GLubyte b;
	GLubyte c;
};

class Skybox : public GameObject
{
public:
	Skybox(void);
	~Skybox(void);
	void skyboxFace(GLenum target, const Image& image);
	GLuint loadSkybox();
	void drawTriangles(int count, Triangle* triangles, Vertex* vertices);
	void update();
	void initSkybox();
	void renderSkybox();
private:
	const char* cubemapTextures[6];
	GLuint textures;
	GLuint skyboxTexture;
};

#endif