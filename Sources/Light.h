/*
 * Light.h
 * Authors: Beatriz Bodas Rodriguez
 * Created: Jan. 2014
 */

#ifndef __LIGHT_H__
#define __LIGHT_H__

#include <GL/glew.h>
#include <GL/freeglut.h>

class Light
{
public:
	Light(void);
	virtual ~Light(void);
	void sunLight();
};

#endif