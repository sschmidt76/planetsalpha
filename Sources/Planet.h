/*
 * Planet.h
 * Authors: Beatriz Bodas Rodriguez
 * Created: Nov. 2013
 */

#ifndef __PLANET_H__
#define __PLANET_H__

#include "GameObject.h"

class Planet : public GameObject
{
public:
	Planet(void);
	Planet (float x, float y, float z, double radius, int slices, int stack);
	virtual ~Planet(void);
	void update();
	void initPlanet();
	GLuint planetTexture;
	GLuint getPlanetTexture();
	GLuint loadPlanet();
	float day, rotationValue, colorValueR,colorValueG,colorValueB;
	double speed;
};

#endif




