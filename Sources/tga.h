/*
 * Author: Sven Reinck
 */

#ifndef __TGA_H__
#define __TGA_H__

#include <GL/glut.h>

struct Image
{
	GLubyte components;
	GLushort width;
	GLushort height;
	GLenum format;
	GLubyte* data;
};

class TGA
{
public:
	static const char* loadTGA(const char* filename, struct Image* image);
	static GLuint toTexture(struct Image* image, GLboolean mipmapping);
};

#endif