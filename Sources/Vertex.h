/*
 * Author: Sven Reinck
 */

#ifndef __VERTEX_H__
#define __VERTEX_H__

#include <math.h>

class Vertex
{
public:
	Vertex(void);
	Vertex(float x, float y, float z);
	virtual ~Vertex(void);
	Vertex operator/(float d) const;
	float length() const;
	Vertex norm() const;
	float x, y, z;
};

#endif

	Vertex operator+(const Vertex& a, const Vertex& b);
	Vertex operator-(const Vertex& a, const Vertex& b);
	Vertex operator*(float s, const Vertex& a);
	Vertex operator%(const Vertex& a, const Vertex& b);