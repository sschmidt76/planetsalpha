/*
 * CreditsScreen.cpp
 *
 *  Created on: 01.2014
 *      Author: Stefanie Schmidt
 */

#include "CreditsScreen.h"

/**
 * CreditsScreen --> Constructor inherits MenuScreen
 * adds the text that is shown on the screen
 * @param void
 */
CreditsScreen::CreditsScreen() : MenuScreen("Credits:")
{
	addItem("1. Back");
	addItem("Beatriz Bodas Rodriguez");
	addItem("Sebastian Pommerening");
	addItem("Stefanie Schmidt");
}

/**
 * CreditsScreen standard destructor
 * @return {void}
 */
CreditsScreen::~CreditsScreen(void)
{
}

/**
 * const Event Handler: go back to main menu

 * @param {MenuManager&} manager as reference
 * @param {int} index
 * @return {void}
 */
void CreditsScreen::onSelect(MenuManager& manager, int index) const
{
	switch (index)
	{
	case 1:
		manager.popScreen();
		break;
	}
}