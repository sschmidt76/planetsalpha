#pragma once
#include <GL\glew.h>
#include <GL\freeglut.h>

#include "gameobject.h"

class PlayerShip : public GameObject
{
public:
	PlayerShip(void);
	virtual ~PlayerShip(void);

	void render();
	void update();
};

