/*
 * Sun.cpp
 *
 *  Created on: 11.2013
 *      Author: Beatriz B. Rodríguez
 */

#include "Sun.h"

GLuint sunShader;
GLuint sunTexture;
GLuint textureId = 1;

/**
 * Sun --> Constructor inherits GameObject and it is added to the GameObject List
 * includes all variables to build sun
 * @param void
 */
Sun::Sun(void):GameObject()
{
	this->x = 0;
	this->y = 0;
	this->z = 0;
	year = 0.0f;
	this->radius = 2;
	this->slices = 40;
	this->stack = 20;
	this->sunTexture = 0;
	
}

/**
 * Sun --> Destructor
 * @param void
 */
Sun::~Sun(void)
{
}

/**
 * initSun function loads vertex and fragment Shader
 * @param {void}
 * @return {void}
 */
void Sun::initSun()
{
	sunShader = Shader::loadShader("Shaders/Sun.vert", "Shaders/Sun.frag");
	
}

/**
 * GLuint function load textures for the sun
 * @param {param}
 * @return {GLuint} textureId;
 */
GLuint Sun::loadSun()
{
	Image sun;

	TGA::loadTGA(("Textures/Planets/TGA/WaterPlain0024_7_S.tga"), &sun);
	
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sun.width, sun.height, 0, GL_RGB, GL_UNSIGNED_BYTE, sun.data ); 
	
	this->sunTexture = textureId;

	return textureId;
}

/**
 * update function with information about position, movement, rendering, connection to shader and texture binding
 * further comments in base class
 * @param {void}
 * @return {void}
 */
void Sun::update()
{
	GLUquadric *quad;
 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	quad = gluNewQuadric();
	 
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, sunTexture);
	glUseProgram(sunShader);
	year += 0.1f;

	GLfloat mat_shininess[] = {180.0};
	GLfloat light_position[] = {2.0, 0.0, 0.0};
	glShadeModel(GL_SMOOTH);
	GLfloat mat_specular[] = {1.0, 1.0, 0.0, 0.5};
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);

	
	glPushMatrix();
	glRotatef((GLfloat) year, 0.0f, 1.0f, 0.0f);
	glRotatef((GLfloat) 90, 1.0f, 0.0f, 0.0f);
	glColor3f (1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, textureId);
	gluQuadricTexture(quad,1);
	gluSphere(quad,radius,slices,stack);
	glPopMatrix();
	
	glUseProgram(0);
	glDisable(GL_TEXTURE_2D);
}