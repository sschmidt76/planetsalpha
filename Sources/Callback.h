/*
 * Callback.h
 * Authors: Beatriz Bodas Rodriguez, Sebastian Pommerening, Stefanie Schmidt
 * Created: Nov. 2013
 */

#ifndef __CALLBACK_H__
#define __CALLBACK_H__

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <irrKlang.h>

#include "Skybox.h"
#include "Ship.h"
#include "SceneGraph.h"
#include "MainMenu.h"
#include "MenuManager.h"
#include "ControlsScreen.h"
#include "CreditsScreen.h"
#include "MenuScreen.h"
#include "Camera.h"
#include "Spawn.h"

void init();
void reshape (int w, int h);
void display();
void handleInput(unsigned char key, int x, int y);
void handleInputUp(unsigned char key, int x, int y);
void showMenu();

#endif