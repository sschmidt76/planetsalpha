/*
 * SceneGraph.h
 * Authors: Beatriz Bodas Rodriguez, Stefanie Schmidt
 * Created: Nov. 2013
 */

#ifndef __SCENEGRAPH_H__
#define __SCENEGRAPH_H__

#include <vector>

#include "GameObject.h"

class SceneGraph
{
public:
	void update();
	static SceneGraph* getInstance();
	std::vector<GameObject*> gameObjects;
private:
	SceneGraph(void); 
	virtual ~SceneGraph(void);
};

#endif